# cronos - test



## requirements

Для работы необходимы 2 docker образа:  
```sh
docker pull postgres
docker pull dpage/pgadmin4
```  
Так же необходим репозиторий cronodump для импорта существующих БД в .csv формат.  
```sh
git clone https://github.com/alephdata/cronodump.git
```

## quick-start

```sh
docker compose up
```
По адресу http://localhost:5051/  находиться PGAdmin для просмотра БД.  
Host name: postgresql ; username: test ; password : test

! Перед повторением шагов выполнить пункт важно в секции `Извлечение данных`

В cront_test.ipynb описано решение тестового задания.  
Для сохранения данных в формате sql приведены команды в конце секций Postgresql.  